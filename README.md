# METAtron

O **METAtron** é uma aplicação que possibilita a criação de uma API gateway em GO e documentada com base na documentação escrita no estilo **swagger**, e ainda possibilita a modificação da API

## Instalação

TODO

## Sintaxe

A sintaxe lida pelo metatron é a do [Swagger]() com alguns campos extra que serão descritos abaixo.

* Atenção: Colocar os campos extras no meio da documentação impossibilita tools como o Apiary de gerar a documentação, pois a sintaxe do Swagger não permite campos extras :( 

Agora vamos descrever os campos nos diversos niveis da documentação.


### Nivel Global

Esse é o nivel mais externo, nesse nivel temos os seguintes campos:
	
- **metatron** : esta é a versão da linguagem que o metatron deve esperar durante o arquivo, seu valor é uma string e deve seguir [este](http://semver.org/) padrão, por exemplo:
```yaml
metatron: "0.1.0"
```
- **prefix**: este é o prefixo global que a API externa deve utilizar, seu valor é uma string que segue o padrão de [URL](https://url.spec.whatwg.org/#url-syntax), por exemplo:
```yaml
metratron: "0.1.0"
prefix: api     # este é um valor valido
prefix: api/v1  # e este também, mas apenas podemos ter um
```
Ainda é possivel especificar comportamentos em relação ao campo TODO do padrão Swagger, como *overwrite*, *prepend* e *append*. No caso de *overwrite* o campo prefix será usado no lugar do TODO, no caso de *prepend* o campo prefix será usado antes do campo TODO e no caso de *append* o campo TODO será usado após ao campo TODO. Especificamos este comportamento como abaixo:
```yaml
metratron: "0.1.0"
prefix:
  value: api     
  behavior: prepend
```
- **accept**: é o campo que define qual são os tipos de conteudo que são aceitos tanto pela API Gateway como os pela quais ela irá se comunicar com a API interna. Pro exemplo:
```yaml
metraton: "0.1.0"
accept:
  gateway: 
    - json
	- xml
	- yaml
  internal:
	- json
```
Note que os tipos não precisam ser os mesmos, por exemplo a API Gateway pode aceitar requisições no formato JSON, mas a externa apenas XML.

- **auth**: este é o mecanismo de segurança que será usado globalmente pela API, o valor desse campo é uma lista, onde a ordem dela define a precedencia dos mecanismos. Pro exemplo:

```yaml
auth: 
  - x-token
  - basic-gi
```
Essa tag substituirá completamente a tag *securityDefinitions* do Swagger. Logo, toda a definição deve ser feita nela globalmente ou dentro de cada path ou método.

### Nivel de endpoints
Este é o nivel que controla cada endpoint. Seus atributos são

- **alias**: este atributo substitui nome especificado no swagger, mas ainda sofre influência do **prefix** visto acima. Por exemplo:
```yaml
swagger: "2.0"
info:
  version: "1.0"
  title: "Hello World API"
paths:
  /hello/{user}:
    alias: v1
    get:
      description: Returns a greeting to the user!
      parameters:
        - name: user
          in: path
          type: string
          required: true
          description: The name of the user to greet.
      responses:
        200:
          description: Returns the greeting.
          schema:
            type: string
        400:
          description: Invalid characters in "user" were provided.
```
- **omit: este parametro define se o endpoint em questão será mostrado na API Gateway ou não, seu valor é um booleano. Por exemplo:
```yaml
swagger: "2.0"
info:
  version: "1.0"
  title: "Hello World API"
paths:
  /hello/{user}:
    alias: v1
    omit: true
    get:
      description: Returns a greeting to the user!
      parameters:
        - name: user
          in: path
          type: string
          required: true
          description: The name of the user to greet.
      responses:
        200:
          description: Returns the greeting.
          schema:
            type: string
        400:
          description: Invalid characters in "user" were provided.
```
- auth: este atributo define o mecanismo de autenticação para esse endpoint especificamente, também é possivel descrever seu comportamento em relação ao campo **auth** global, como *overwrite*, *prepend* e *append*. No caso de *overwrite* o campo prefix será usado no lugar do **auth** global, no caso de *prepend* o campo prefix será usado antes do campo **auth** global e no caso de *append* o campo **auth** será usado após ao campo **auth** global. Especificamos este comportamento como abaixo:
```yaml
swagger: "2.0"
info:
  version: "1.0"
  title: "Hello World API"
paths:
  /hello/{user}:
    alias: v1
    omit: true
    auth: 
      type: X-Token
      behavior: overwrite
    get:
      description: Returns a greeting to the user!
      parameters:
        - name: user
          in: path
          type: string
          required: true
          description: The name of the user to greet.
      responses:
        200:
          description: Returns the greeting.
          schema:
            type: string
        400:
          description: Invalid characters in "user" were provided.
```

### Nivel de metodo de um endpoint
Este é o nivel que controla cada metodo dentro de um endpoint. Seus atributos são

- **omit**: funciona exatamente como o omit do endpoint, exceto que este omit só tem algum valor caso o omit to endpoint esteja falso. Exmeplo: 
```yaml
swagger: "2.0"
info:
  version: "1.0"
  title: "Hello World API"
paths:
  /hello/{user}:
    alias: v1
    omit: false
    auth: 
      type: X-Token
      behavior: overwrite
    get:
      omit: true
      description: Returns a greeting to the user!
      parameters:
        - name: user
          in: path
          type: string
          required: true
          description: The name of the user to greet.
      responses:
        200:
          description: Returns the greeting.
          schema:
            type: string
        400:
          description: Invalid characters in "user" were provided.
```
- **auth**: funciona exatemente como o do endpoint.
```yaml
swagger: "2.0"
info:
  version: "1.0"
  title: "Hello World API"
paths:
  /hello/{user}:
    alias: v1
    omit: false
    auth: 
      type: X-Token
      behavior: overwrite
    get:
      omit: true
      auth: 
        type: X-Token
        behavior: overwrite
      description: Returns a greeting to the user!
      parameters:
        - name: user
          in: path
          type: string
          required: true
          description: The name of the user to greet.
      responses:
        200:
          description: Returns the greeting.
          schema:
            type: string
        400:
          description: Invalid characters in "user" were provided.
```

### Uso

TODO